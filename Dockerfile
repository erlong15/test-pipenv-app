FROM python:3.7-slim
COPY ./src /app
WORKDIR /app
RUN pip install pipenv && \
    pipenv install --ignore-pipfile
ENTRYPOINT ["pipenv", "run", "python", "server.py"]

