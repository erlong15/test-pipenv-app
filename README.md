Task
====

Given simple counter application that uses Redis as a backend storage to keep 
the persistent counter.

#### API:

    GET / - returns current counter number
    POST /increment - increments the counter
    DELETE /increment - resets the counter

#### Run configuration:

`server.py` - main application entrypoint. 
Server can be started via command: `pipenv run python server.py`

Dependencies
------------

- All the python dependencies MUST be installed via pipenv
- Application uses Redis as a backend storage

Expectations
------------

- Dockerfile is provided as application run environment
- Application running in Docker Swarm / Kubernetes stack / etc. (feel free to choose orchestrator)
- Stack can be deployed locally running single bash script / command (prerequisites provided if something needs to be installed)
- Application is located behind load-balancer and 
can be reachable via domain name from host (e.g. http://counter-app.local)
- Application is scalable and fault tolerant (killing single node will not result into application is down)
- Application can be easily configured via ENV variables (feel free to propose updates into application if needed)

Solution
------------

- was created GitLab repository
- was fixed the code for using a password to redis as an environment variable
- was created Dockerfile
- was created docker-compose.yml for testing purpose (without replication)
- was built a docker image and pushed to registry in gitlab
- the image is registry.gitlab.com/erlong15/test-pipenv-app:latest
- was created a deployment yaml file for deployment to GKE
- `kubectl apply -f k8s/app-k8s-deployment.yml`
- for connecting to service after deploy you should check services:
    - `kubectl get svc`
    - find out external ip for 'counter' service
    - write into your /etc/hosts
        - <service-ip> counter-app.local
    - connect to service
        - `curl http://counter-app.local`
        - `curl -X POST http://counter-app.local/increment` 
        - `curl -X DELETE http://counter-app.local/increment` 
- you can kill any node in your k8s cluster (except a node with redis) - and app should work fine
